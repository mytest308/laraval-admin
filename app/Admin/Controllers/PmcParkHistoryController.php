<?php

namespace App\Admin\Controllers;

use App\Models\PmcParkingEntitys;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PmcParkHistoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->edit_form()->edit($id));
        });
    }

    protected function edit_form()
    {
        return Admin::form(PmcParkingEntitys::class, function (Form $form) {
            $form->hidden('pmc_id')->default(Admin::user()->pmc_id);
            $form->saving(function(Form $form){
                $form->pmc_id = Admin::user()->pmc_id;
            });
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(PmcParkingEntitys::class, function (Grid $grid) {
            $grid->Id("序号");

            $this->add_pmc_id_condition($grid);
            $grid->created_at('创建时间')->format();

            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->disableDelete();
            });
            $grid->disableRowSelector();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(PmcParkingEntitys::class, function (Form $form) {
            $form->hidden('pmc_id')->default(Admin::user()->pmc_id);
            $form->saving(function(Form $form){
                $form->pmc_id = Admin::user()->pmc_id;
            });
        });
    }
}
