<?php

namespace App\Admin\Controllers;

use App\Models\PmcParks;
use App\Models\Provinces;
use App\Models\Cities;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;

class PmcParksController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('header');
            $content->description('description'); 
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $checkDataRbac = $this->checkDataRbac(PmcParks::class, $id);
            if($checkDataRbac !== true){
                return $checkDataRbac;
            }

            $content->header('header');
            $content->description('description');
            $content->body($this->edit_form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('header');
            $content->description('description');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(PmcParks::class, function (Grid $grid) {
            $grid->id('序号')->sortable();
            $grid->park_name('名称');
            $grid->column('province.name', '省份');
            $grid->column('city.name', '城市');
            $grid->address('地址');
            $grid->coupon_type('折扣类型')->coupon_type_text();
            $grid->contact_user('联系人');
            $grid->contact_phone('联系电话');
            $grid->fixed_parking_space_count('固定车位数')->default(0);
            $grid->temp_parking_space_count('临停车位数')->default(0);
            $grid->created_at('创建时间')->format();

            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });

            // filter($callback)方法用来设置表格的简单搜索框
            $grid->filter(function ($filter) {
                $filter->useModal();
                $filter->between('created_at', 'Created Time')->datetime();
            });

            $grid->disableRowSelector();

            $this->add_pmc_condition($grid);

            //more sql condition
            $grid->model()->orderby("id","desc");
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(PmcParks::class, function (Form $form) {
            $form->saved(function (Form $form) {
                $form->pmc_id = 44;
                $form->create_by = 44;
                $form->modify_by = 44;
                $form->save();
            });

            $form->tab('基本信息', function ($form) {
                $form->hidden('pmc_id')->default(12);
                $form->text('park_name', '停车场名称')->rules('required');
                
                $form->select('province_id', '省份')->options(
                    Provinces::where('id', '>', 1)->pluck('name', 'id')
                )->rules('required');
                
                $form->select('city_id', '城市')->options(['' => '请选择']);

                $form->text('address', '地址');
                $form->text('contact_user', '联系人');
                $form->text('contact_phone', '联系电话');
                $form->text('longitude', '经度');
                $form->text('latitude', '纬度');

            })->tab('车场设置', function ($form) {
                $form->select('coupon_type', '折扣类型')->options(PmcParks::$coupon_type);
                $form->number('fixed_parking_space_count', '固定车车位数')->default(0);
                $form->number('temp_parking_space_count', '临时车车位数')->default(0);
                $form->number('early_warning_num', '固定车提前预警天数')->default(0);
                $form->select('tmp_multi_in_out', '临时车是否允许多次进出')->options(status_text(null))->default(1);
                $form->select('fix_multi_in_out', '固定车是否允许多次进出')->options(status_text(null))->default(1);
                $form->select('temporary_car_control_flag', '断网时临时车准入控制标志')
                    ->options(PmcParks::$temporary_car_control_flag)
                    ->default(2);

                $form->select('allow_car_types', '允许进场的车类型')->options(PmcParks::$allow_car_types);

                $form->number('max_duration_time_to_leave', '缴费后必须在规定时间(分钟)内离场');
            })->tab('银行信息', function ($form){
                $form->number('settle_account_period', '结算周期')->default(7);
                $form->text('bank_name', '开户银行');
                $form->text('bank_accout_name', '开户人名字');
                $form->text('bank_card_number', '接受结算停车费的银行卡');
                $form->hidden('create_by')->default(1);
                $form->hidden('modify_by')->default(1);
            });
        });
    }

    protected function edit_form()
    {
        return Admin::form(PmcParks::class, function (Form $form) {
            $form->saving(function(Form $form){
                $row = PmcParks::find($form->id);
                if($row->pmc_id != Admin::user()->id){
                    return responseWarning('你没有权限操作');
                }
            });

            $form->tab('基本信息', function ($form) {
                $form->display('park_name', '停车场名称');
                $form->select('province_id', '省份')->options(
                    Provinces::where('id', '>', 1)->pluck('name', 'id')
                )->rules('required');

                $form->select('city_id', '城市')->options(['' => '请选择']);
                $form->text('address', '地址');
                $form->text('contact_user', '联系人');
                $form->text('contact_phone', '联系电话');
                $form->text('longitude', '经度');
                $form->text('latitude', '纬度');

            })->tab('车场设置', function ($form) {
                $form->select('coupon_type', '折扣类型')->options(PmcParks::$coupon_type);
                $form->number('fixed_parking_space_count', '固定车车位数')->default(0);
                $form->number('temp_parking_space_count', '临时车车位数')->default(0);
                $form->number('early_warning_num', '固定车提前预警天数')->default(0);
                $form->select('tmp_multi_in_out', '临时车是否允许多次进出')->options(status_text(null))->default(1);
                $form->select('fix_multi_in_out', '固定车是否允许多次进出')->options(status_text(null))->default(1);
                $form->select('temporary_car_control_flag', '断网时临时车准入控制标志')
                    ->options(PmcParks::$temporary_car_control_flag)
                    ->default(2);

                $form->select('allow_car_types', '允许进场的车类型')->options(PmcParks::$allow_car_types);

                $form->number('max_duration_time_to_leave', '缴费后必须在规定时间(分钟)内离场');
            })->tab('银行信息', function ($form){
                $form->display('settle_account_period', '结算周期');
                $form->display('bank_name', '开户银行');
                $form->display('bank_accout_name', '开户人名字');
                $form->display('bank_card_number', '接受结算停车费的银行卡');
                $form->hidden('modify_by')->default(1);
                $form->hidden('id');
            });
        });
    }
}
