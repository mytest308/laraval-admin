<?php
use Illuminate\Http\Request;
use Illuminate\Routing\Router;

Route::get('/', function(){
    return response()->redirectTo('/admin');
});

Admin::registerHelpersRoutes();

Route::get('/', function(){
    return response()->redirectTo('/admin');
});

Route::group([
    'prefix'        => config('admin.prefix'),
    'namespace'     => Admin::controllerNamespace(),
    'middleware'    => ['web', 'admin'],
], function (Router $router) {
    $router->get('/', 			            'HomeController@index');
	$router->resource('pmc_parks',          PmcParksController::class);
	$router->resource('pmc_card_infos',     PmcCardInfosController::class);
	$router->resource('pmc_park_history',   PmcParkHistoryController::class);
});

Route::get('/admin/city/list', function(Request $request){
    $id = $request->get('id');
	$list = \App\Models\Cities::where('province_id', $id)
        ->where('status', 1)
        ->paginate(null, ['id', 'name as text']);
	return $list->items();
});