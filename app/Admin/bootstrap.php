<?php
use Encore\Admin\Grid\Column;
/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

Encore\Admin\Form::forget(['map', 'editor']);

Column::extend('format', function ($value, $format = 'Y-m-d') {
	if(!is_numeric($value)){
		$value = strtotime($value);
	}
    return date($format, $value);
});

Column::extend('status_text', function ($value, $default = 0) {
	$text = ['否', '是'];
	return !empty($text[$value]) ? $text[$value] : $text[$default];
});

Column::extend('coupon_type_text', function ($value, $default = 0) {
	$text = ['时间', '金钱'];
	return !empty($text[$value]) ? $text[$value] : $text[$default];
});

Column::extend('default', function ($value, $default = 0) {
	if($value === '' || is_null($value)){
		return $default;
	}
	return $value;
});