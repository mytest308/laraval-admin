<?php namespace App\Listeners;

/**
 *   记录程序运行时的Sql语句
 */
use Illuminate\Support\Facades\Log;
use DateTime;

class QueryListener {

    public function __construct()
    {

    }

    /**
     * 根据 配置参数 LOG_SQL 值等于 true时，则执行记录
     *
     * @param  Events  $event
     * @return void
     */
    public function handle($sql, $params)
    {
        if (env("LOG_SQL")) {
            dd(22);
            foreach ($params as $index => $param) {
                if ($param instanceof DateTime) {
                    $params[$index] = $param->format('Y-m-d H:i:s');
                }
            }
            $sql = str_replace("?", "'%s'", $sql);

            array_unshift($params, $sql);

            Log::info( 'querysql:'. call_user_func_array('sprintf', $params));
        }
    }

}