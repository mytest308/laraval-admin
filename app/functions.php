<?php
function status_text($val = 0)
{
    $status = ['否', '是'];
    if(is_null($val)){
        return $status;
    }
    return !empty($status[$val]) ? $status[$val] : $status[0];
}

function responseWarning($message = '')
{
    $error = new \Illuminate\Support\MessageBag([
        'title'   => '提示',
        'message' => $message,
    ]);
    return back()->with(compact('error'));
}