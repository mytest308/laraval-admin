<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Provinces;
use App\Models\Cities;

class PmcParks extends Model
{
    protected $table = 'pmc_parks';

    public static $coupon_type = [
        '1' => '时间',
        '2' => '金钱',
    ];

    public static $temporary_car_control_flag = [
        0 => '不允许进场',
        1 => '允许进场(不需要人工确认)',
        2 => '允许进场，但需要人工确认',
    ];

    public static $allow_car_types = [
        '所有车辆',
        '只允许月卡车进场'
    ];

    public function province()
    {
    	return $this->belongsTo(Provinces::class, 'province_id', "id");
    }

    public function city()
    {
    	return $this->belongsTo(Cities::class, 'city_id', "id");
    }
}