<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChannelOlds extends Model
{
    protected $table = 'pmc_park_channel_old';
}