<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRecords extends Model
{
    protected $table = 'pay_records';
}