<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChargeModes extends Model
{
    protected $table = 'pmc_park_charge_mode';
}