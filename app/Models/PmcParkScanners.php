<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkScanners extends Model
{
    protected $table = 'pmc_park_scanner';
}