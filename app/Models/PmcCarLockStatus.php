<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcCarLockStatus extends Model
{
    protected $table = 'pmc_car_lock_status';
}