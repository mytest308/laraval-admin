<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenus extends Model
{
    protected $table = 'admin_menu';
}