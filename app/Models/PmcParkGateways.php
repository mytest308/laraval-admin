<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkGateways extends Model
{
    protected $table = 'pmc_park_gateway';
}