<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUserRoles extends Model
{
    protected $table = 'admin_user_role';
}