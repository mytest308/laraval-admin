<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcCardInfos extends Model
{
    protected $table = 'pmc_card_info';
}