<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BluetoothCards extends Model
{
    protected $table = 'bluetooth_card';
}