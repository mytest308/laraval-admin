<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopShops extends Model
{
    protected $table = 'pmc_shop_shops';
}