<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcNodeOfflineWarnings extends Model
{
    protected $table = 'pmc_node_offline_warning';
}