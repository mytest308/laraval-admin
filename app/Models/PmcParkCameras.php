<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkCameras extends Model
{
    protected $table = 'pmc_park_camera';
}