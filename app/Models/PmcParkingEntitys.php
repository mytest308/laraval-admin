<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkingEntitys extends Model
{
    protected $primaryKey = 'Id';
    protected $table = 'pmc_parking_entity';
}