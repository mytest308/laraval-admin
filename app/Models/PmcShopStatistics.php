<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopStatistics extends Model
{
    protected $table = 'pmc_shop_statistics';
}