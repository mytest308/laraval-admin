<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcAgentWithdrawals extends Model
{
    protected $table = 'pmc_agent_withdrawals';
}