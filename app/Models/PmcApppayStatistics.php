<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcApppayStatistics extends Model
{
    protected $table = 'pmc_apppay_statistics';
}