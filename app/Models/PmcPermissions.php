<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcPermissions extends Model
{
    protected $table = 'pmc_permissions';
}