<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenuPermissions extends Model
{
    protected $table = 'admin_menu_permissions';
}