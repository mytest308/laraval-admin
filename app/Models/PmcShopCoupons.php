<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopCoupons extends Model
{
    protected $table = 'pmc_shop_coupons';
}