<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrepaidCardAgentReports extends Model
{
    protected $table = 'prepaid_card_agent_report';
}