<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkingInInfields extends Model
{
    protected $table = 'pmc_parking_in_infield';
}