<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChargeTimespans extends Model
{
    protected $table = 'pmc_park_charge_timespan';
}