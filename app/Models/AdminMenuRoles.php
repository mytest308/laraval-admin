<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenuRoles extends Model
{
    protected $table = 'admin_menu_roles';
}