<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcNodeRecoverEvents extends Model
{
    protected $table = 'pmc_node_recover_event';
}