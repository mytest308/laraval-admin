<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Provinces;

class Cities extends Model
{
    protected $table = 'cities';

    public function province()
    {
        return $this->belongsTo(Provinces::class, 'province_id');
    }

    public static function province_map_city()
    {
        $lists = self::where('status', 1)->get();
        $tmp = [];
        foreach ($lists as $list){
            if(!isset($tmp[$list->province_id])){
                $tmp[$list->province_id] = [];
            }
            $tmp[$list->province_id][] = [
                'id' => $list->id,
                'text' => $list->name
            ];
        }
        return $tmp;
    }
}