<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrepaidCardReports extends Model
{
    protected $table = 'prepaid_card_report';
}