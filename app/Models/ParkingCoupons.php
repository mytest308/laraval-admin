<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingCoupons extends Model
{
    protected $table = 'parking_coupons';
}