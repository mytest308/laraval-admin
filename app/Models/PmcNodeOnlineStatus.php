<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcNodeOnlineStatus extends Model
{
    protected $table = 'pmc_node_online_status';
}