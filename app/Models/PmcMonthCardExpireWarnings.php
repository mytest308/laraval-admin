<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcMonthCardExpireWarnings extends Model
{
    protected $table = 'pmc_month_card_expire_warning';
}