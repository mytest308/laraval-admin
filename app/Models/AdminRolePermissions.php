<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRolePermissions extends Model
{
    protected $table = 'admin_role_permissions';
}