<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChannels extends Model
{
    protected $table = 'pmc_park_channel';
}