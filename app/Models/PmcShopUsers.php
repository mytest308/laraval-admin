<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopUsers extends Model
{
    protected $table = 'pmc_shop_user';
}