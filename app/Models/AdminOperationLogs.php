<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminOperationLogs extends Model
{
    protected $table = 'admin_operation_log';
}