<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChargeNames extends Model
{
    protected $table = 'pmc_park_charge_name';
}