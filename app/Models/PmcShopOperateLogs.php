<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopOperateLogs extends Model
{
    protected $table = 'pmc_shop_operate_log';
}