<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcCardValidDateRanges extends Model
{
    protected $table = 'pmc_card_valid_date_range';
}