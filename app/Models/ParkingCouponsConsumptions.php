<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingCouponsConsumptions extends Model
{
    protected $table = 'parking_coupons_consumption';
}