<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkWorkstations extends Model
{
    protected $table = 'pmc_park_workstation';
}