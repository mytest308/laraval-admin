<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcPayLogs extends Model
{
    protected $table = 'pmc_pay_logs';
}