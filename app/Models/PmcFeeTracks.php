<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcFeeTracks extends Model
{
    protected $table = 'pmc_fee_track';
}