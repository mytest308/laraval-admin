<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcBarrierUpRecords extends Model
{
    protected $table = 'pmc_barrier_up_record';
}