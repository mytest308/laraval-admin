<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcSystemLogs extends Model
{
    protected $table = 'pmc_system_logs';
}