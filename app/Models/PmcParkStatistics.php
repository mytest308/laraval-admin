<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkStatistics extends Model
{
    protected $table = 'pmc_park_statistics';
}