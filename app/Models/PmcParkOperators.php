<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkOperators extends Model
{
    protected $table = 'pmc_park_operator';
}