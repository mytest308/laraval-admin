<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcAgents extends Model
{
    protected $table = 'pmc_agents';
}