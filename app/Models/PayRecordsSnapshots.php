<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRecordsSnapshots extends Model
{
    protected $table = 'pay_records_snapshot';
}