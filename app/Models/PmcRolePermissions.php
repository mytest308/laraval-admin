<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcRolePermissions extends Model
{
    protected $table = 'pmc_role_permission';
}