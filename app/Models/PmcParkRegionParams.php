<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkRegionParams extends Model
{
    protected $table = 'pmc_park_region_params';
}