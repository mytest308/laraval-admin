<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcRoles extends Model
{
    protected $table = 'pmc_roles';
}