<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JoyexpConfigs extends Model
{
    protected $table = 'joyexp_config';
}