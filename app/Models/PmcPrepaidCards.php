<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcPrepaidCards extends Model
{
    protected $table = 'pmc_prepaid_card';
}