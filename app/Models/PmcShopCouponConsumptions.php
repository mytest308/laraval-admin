<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcShopCouponConsumptions extends Model
{
    protected $table = 'pmc_shop_coupon_consumption';
}