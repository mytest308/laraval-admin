<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SnapshotInfos extends Model
{
    protected $table = 'snapshot_infos';
}