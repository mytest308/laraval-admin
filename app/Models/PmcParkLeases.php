<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkLeases extends Model
{
    protected $table = 'pmc_park_lease';
}