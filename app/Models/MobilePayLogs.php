<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobilePayLogs extends Model
{
    protected $table = 'mobile_pay_log';
}