<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkLedInfos extends Model
{
    protected $table = 'pmc_park_led_info';
}