<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcManuallyBarrierUpRecords extends Model
{
    protected $table = 'pmc_manually_barrier_up_record';
}