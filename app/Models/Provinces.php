<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cities;

class Provinces extends Model
{
    protected $table = 'provinces';

    public function city()
    {
        return $this->hasMany(Cities::class, 'province_id');
    }
}