<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientInfos extends Model
{
    protected $table = 'client_infos';
}