<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmcParkChargeNormals extends Model
{
    protected $table = 'pmc_park_charge_normal';
}