<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppCarLocks extends Model
{
    protected $table = 'app_car_locks';
}