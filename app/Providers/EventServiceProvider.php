<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'illuminate.query' => [
            'App\Listeners\QueryListener'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        DB::listen(function ($event) {
            $params = $event->bindings;
            $sql = $event->sql;
            foreach ($params as $index => $param) {
                if ($param instanceof DateTime) {
                    $params[$index] = $param->format('Y-m-d H:i:s');
                }
            }
            $sql = str_replace("?", "'%s'", $sql);

            array_unshift($params, $sql);

            Log::info( 'querysql:'. call_user_func_array('sprintf', $params));
        });
    }
}
