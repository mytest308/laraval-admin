<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class model extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'build all tables model file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lists = DB::select("show tables");
        $db_key = 'Tables_in_'.env('DB_DATABASE');
        
        foreach ($lists as $list) {
            if(stripos($list->$db_key, 'sym_') === 0){
                continue;
            }

            if(stripos($list->$db_key, 'sync_') === 0){
                continue;
            }

            if(stripos($list->$db_key, 'symmtric_') === 0){
                continue;
            }

            $arrs = explode('_', $list->$db_key);
            foreach ($arrs as &$arr) {
                $arr = ucfirst($arr);
            }

            $className = trim(implode('', $arrs), 's').'s';

            if(!file_exists(app_path().'/Models/'.$className.'.php')){

                if(!file_exists(app_path().'/Models/')){
                    mkdir(app_path().'/Models/', 0777, true);
                }

                echo 'create '.app_path().'/Models/'.$className.'.php'."\n";
                $content = file_get_contents(public_path().'/model.txt');
                $content = str_replace('{$model}', $className, $content);
                $content = str_replace('{$table}', $list->$db_key, $content);
                file_put_contents(app_path().'/Models/'.$className.'.php', $content);
            }
        }
        echo "all done";
    }
}
