@php
$_citys = \App\Models\Cities::province_map_city();
@endphp

<script>
var $city_list = {!! json_encode($_citys, JSON_UNESCAPED_UNICODE) !!};
$(function(){
    var $province_obj = $("select[name='province_id']");
    change_city($province_obj.val());
    $province_obj.select2().on('change', function(){
        change_city($(this).val());
    });
});

function change_city($provice_id)
{
    var $obj = $("select[name='city_id']");
    $obj.html('').select2({
        data:$city_list[$provice_id],
    });
}
</script>