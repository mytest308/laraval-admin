<?php

namespace Encore\Admin\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make empty admin controller';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->modelExists()) {
            $this->error('Model does not exists !');

            return false;
        }

        //$this->type = $this->parseName($this->getNameInput());

        parent::fire();
    }

    /**
     * Determine if the model is exists.
     *
     * @return bool
     */
    protected function modelExists()
    {
        $model = $this->option('model');

        if (empty($model)) {
            return true;
        }

        if(!stristr($model, '\\')){
            $model = 'App\Models\\'.$model;
        }

        return class_exists($model);
    }

    protected function getColumns()
    {
        $model = $this->getModelClass();
        $lists = [];
        if(class_exists($model)){
            $table = (new $model())->getTable();
            $lists = DB::select(sprintf(
                "SELECT column_name,data_type,column_comment FROM `information_schema`.`COLUMNS` WHERE TABLE_SCHEMA='%s' AND TABLE_NAME ='%s'"
                ,env('DB_DATABASE')
                ,$table
            ));
        }
        return $lists;
    }

    protected function getGrids()
    {
        $columns = $this->getColumns();
        $grids = [];
        foreach ($columns as $list) {
            if(stristr($list->data_type, 'time')){
                $grids[] = '$grid->'.$list->column_name.'("'.$list->column_comment.'")->format();';
            }else{
                $grids[] = '$grid->'.$list->column_name.'("'.$list->column_comment.'");';
            }
        }
        return implode("\n", $grids);
    }

    protected function getForms()
    {
        $columns = $this->getColumns();
        $grids = [];
        foreach ($columns as $list) {
            if(stristr($list->data_type, 'int')){
                $grids[] = '$form->number("'.$list->column_name.'", "'.$list->column_comment.'");';
            }else if(stristr($list->data_type, 'time')){
                $grids[] = '$form->date("'.$list->column_name.'", "'.$list->column_comment.'");';
            }else{
                $grids[] = '$form->text("'.$list->column_name.'", "'.$list->column_comment.'");';
            }
        }
        return implode("\n", $grids);
    }

    protected function getModelClass()
    {
        $model = $this->option('model');
        if(!stristr($model, '\\')){
            $model = 'App\Models\\'.$model;
        }
        return $model;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $model = $this->option('model');
        if(!stristr($model, '\\')){
            $model = 'App\Models\\'.$model;
        }

        return str_replace(
            ['DummyModelNamespace', 'DummyModel', '{$grid_columns}', '{$form_columns}'],
            [$model, class_basename($model), $this->getGrids(), $this->getForms()],
            $stub
        );
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('model')) {
            return __DIR__.'/stubs/controller.stub';
        }

        return __DIR__.'/stubs/blank.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $directory = config('admin.directory');

        $namespace = ucfirst(basename($directory));

        return $rootNamespace."\\$namespace\Controllers";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', null, InputOption::VALUE_REQUIRED,
                'The eloquent model that should be use as controller data source.', ],
        ];
    }
}
