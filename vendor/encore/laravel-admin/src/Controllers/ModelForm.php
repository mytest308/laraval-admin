<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Facades\Admin;

trait ModelForm
{
    public function show($id)
    {
        return $this->edit($id);
    }

    public function update($id)
    {
        return $this->form()->update($id);
    }

    public function destroy($id)
    {
        if ($this->form()->destroy($id)) {
            return response()->json([
                'status'  => true,
                'message' => trans('admin::lang.delete_succeeded'),
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'message' => trans('admin::lang.delete_failed'),
            ]);
        }
    }

    public function store()
    {
        return $this->form()->store();
    }

    public function isAdmin()
    {
        return Admin::user()->isAdministrator();
    }

    public function isPmc()
    {
        return $this->isAdmin() ? false : true;
    }

    public function add_pmc_condition($gird)
    {
        if($this->isPmc()) {
            $gird->model()->where('pmc_id', Admin::user()->pmc_id);
        }
    }
}
